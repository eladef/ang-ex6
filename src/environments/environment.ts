// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,  
    firebase: {
      apiKey: "AIzaSyBGn6Yw9E4vng3r_hJh-RB0uMw-YJc0lu4",
      authDomain: "ex6and7.firebaseapp.com",
      databaseURL: "https://ex6and7.firebaseio.com",
      projectId: "ex6and7",
      storageBucket: "ex6and7.appspot.com",
      messagingSenderId: "33369619327"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
