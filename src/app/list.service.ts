import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth'
import {Observable} from 'rxjs';
import {AngularFireDatabase} from '@angular/fire/database';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ListService {


  addList(text:string){
    this.authService.user.subscribe(user=>{
      this.db.list('users/'+user.uid+'/list').push({'text':text, 'status':false});
    })
       }
    
       delete(key){
         this.authService.user.subscribe(user=>{
          this.db.list('users/'+user.uid+'/list').remove(key);
         })
       }

       updatelist(key:string, text:string, status:boolean){
        this.authService.user.subscribe(user =>{
          this.db.list('/users/'+user.uid+'/list').update(key,{'text':text,'status':status});
        })
      }

      updateStatus(key:string, text:string, status:boolean)
      {
        this.authService.user.subscribe(user =>{
          this.db.list('/users/'+user.uid+'/list').update(key,{'text':text, 'status':status});
        })
        
      }


  constructor(private db:AngularFireDatabase, private authService:AuthService) { }
}
