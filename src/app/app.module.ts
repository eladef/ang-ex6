import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { LoginComponent } from './login/login.component';
import { NavComponent } from './nav/nav.component';

//for firebase:
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

//app modules:
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';

//for eviromests:
import {environment} from '../environments/environment';

//for 2 way bunding:
import{FormsModule} from '@angular/forms';
import { Routes, RouterModule } from '@angular/router'; // פקודה להבאת הנתיבים

//for the registration (from Angular material)
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ListComponent } from './list/list.component';
import { OnelistComponent } from './onelist/onelist.component';

import {MatCheckboxModule} from '@angular/material/checkbox';


@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    WelcomeComponent,
    LoginComponent,
    NavComponent,
    ListComponent,
    OnelistComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp (environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    MatCheckboxModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([
      {path:'', component:SignupComponent},
      {path:'login', component:LoginComponent},
      {path:'signup', component:SignupComponent},
      {path:'welcome', component:WelcomeComponent},
      {path:'list', component:ListComponent},
      {path:'**', component:SignupComponent}

  
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
