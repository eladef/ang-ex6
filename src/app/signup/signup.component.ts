import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from "@angular/router";
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  
  email : string;
  password : string;
  name : string;
  error= '';

  signup(){
    // console.log("signup clicked"+' '+this.name+' '+this.password+' '+this.email);
       this.authService.signup(this.email,this.password).then(value=>{
        this.authService.updateProfile(value.user , this.name);
        this. authService.addUser(value.user, this.name);
       }).then(value=>{
         this.router.navigate(['/welcome']);
       }).catch(err =>{
         this.error = err;
         console.log(err);
              
       })
           }

            

  constructor(private authService:AuthService, private router:Router) { }

  ngOnInit() {
  }

}
