import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ListService } from 'src/app/list.service';


@Component({
  selector: 'onelist',
  templateUrl: './onelist.component.html',
  styleUrls: ['./onelist.component.css']
})
export class OnelistComponent implements OnInit {

  @Input() data:any;
  @Output() myButtonClicked= new EventEmitter<any>();

  showtheButton = false; 
  showEditField = false;

tempText;
text;
comment;
id;
key;
status: boolean;

showEdit()
{
  this.tempText = this.text;
  this.showtheButton = false;
  this.showEditField = true;
}

save()
{
  this.listService.updatelist(this.key,this.text, this.status);
  this.showEditField = false;
}

cancel()
{
  this.text = this.tempText;
  this.showEditField = false;
}

checkChange()
{
  this.listService.updateStatus(this.key,this.text,this.status);
}


send(){
  this.myButtonClicked.emit(this.text);
    }

showButton(){
this.showtheButton=true;
}
hideButton(){
this.showtheButton=false;
}

delete(){
this.listService.delete(this.key);
}

  constructor(private listService:ListService) { }

  ngOnInit() {
    this.text = this.data.text;
    this.comment = this.data.comment;
    this.id = this.data.id;

    this.key = this.data.$key;
  }

}
