import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {AuthService} from '../auth.service';
import {ListService} from 'src/app/list.service';
import { Router } from '@angular/router';

@Component({
  selector: 'list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  onelistTextFromlist;

  showText($event){
    this.onelistTextFromlist = ($event);
  }

  list=[];
  text:string;
  row:string;

  ifStatus:boolean; //בדיקה מה הסטטוס שבוחרים בפילטר
  todoStatus:boolean; // סטטוס של טודו חדש שמכניסים
  key;
  taskStatus = [];


  addList(){
    this.listService.addList(this.text);
    this.row="You just add  "+this.text+" to your list";
    this.text='';


  }

  constructor(private db:AngularFireDatabase, private authService:AuthService , private listService:ListService, private router:Router) { }

  ngOnInit() {
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid+'/list').snapshotChanges().subscribe(
        list => {
          this.list=[];
          list.forEach(
            todo =>{
            let y = todo.payload.toJSON();
            y['$key']= todo.key;
            this.list.push(y);  
            }
          )
        }
      )

    })

  }

  toFilter()
  {
    this.authService.user.subscribe(user => {
    this.db.list('/users/'+user.uid+'/list').snapshotChanges().subscribe(
      list =>{
        this.list = [];
        list.forEach(
          todo => {
            let y = todo.payload.toJSON();
            y["$key"] = todo.key;          
              if (this.ifStatus == y['status']) {
                this.list.push(y);
              }
              else if (this.ifStatus != true && this.ifStatus != false)
              {
                this.list.push(y);
              }
              
          }
        )
      }
    )
    })
  }

  showTodo()
  {
    this.authService.user.subscribe(user => {
      this.db.list('/users/' + user.uid + '/list').snapshotChanges().subscribe(
        list => {
          this.list = [];
          list.forEach(
            todo => {
              let y = todo.payload.toJSON();
              y["$key"] = todo.key;
              this.list.push(y);
            }
          )
        }  
      )
    })
  }

}
