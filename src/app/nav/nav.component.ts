import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from '../auth.service';

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  toLogin(){
    this.router.navigate(['/login'])
  }


  toLogout()
  {
    this.authService.logout().
    then(value =>{
    this.router.navigate(['/login'])
    }).catch(err=> {
      console.log(err)
    })
  } 

  toSignup(){
    this.router.navigate(['/signup'])
  }

  toWelcome(){
    this.router.navigate(['/welcome'])
  }

  toList(){
     this.router.navigate(['/list'])
}

  constructor(private router:Router , private authService:AuthService) { }

  ngOnInit() {
  }

}
