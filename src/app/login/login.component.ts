import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from "@angular/router";
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {ListService} from 'src/app/list.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string;
password: string;
error='';

  login(){
    this.authService.login(this.email , this.password).then(value=>{
        this.router.navigate(['welcome']);
        }).catch(err =>{
          this.error = err;
          console.log(err);
  })
  }

  constructor(private authService:AuthService , private router:Router) { }

  ngOnInit() {
  }

}
