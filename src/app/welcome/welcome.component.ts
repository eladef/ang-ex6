import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {AuthService} from '../auth.service';
import {ListService} from 'src/app/list.service';
@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(private db:AngularFireDatabase, private authService:AuthService , private listService:ListService) { }

  ngOnInit() {
  }

}
